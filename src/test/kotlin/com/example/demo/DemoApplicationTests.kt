package com.example.demo

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBody

@SpringBootTest
@AutoConfigureWebTestClient
class DemoApplicationTests(@Autowired private val webTestClient: WebTestClient) {


    //If accept header is added it goes to CustomErrorWebExceptionHandler line number 46
    //Api advising is written for both the test case

    @Test
    fun `handle should map NotAcceptedStatusException exception to custom exception`() {
        webTestClient.get()
                .uri("/test/acceptHeader")
                .header("accept", "text/plain")
                .exchange()
                .expectStatus().isBadRequest
                .expectBody<AnotherExceptionDTO>()
                .returnResult().apply {
                    val exception = responseBody
                    Assertions.assertThat(exception).isNotNull
                    exception!!.messages.apply {
                        Assertions.assertThat(map { it.category }).containsOnly("Error")
                        Assertions.assertThat(map { it.code }).containsOnly("FORMAT_ERROR")
                    }
                }
    }

    // but for this test it does not go to the CustomErrorWebExceptionHandler line number 46
    @Test
    fun `handle should map custom exception to any other exception`() {
        webTestClient.get()
                .uri("/test/customException")
                .exchange()
                .expectStatus().is5xxServerError
                .expectBody<AnotherExceptionDTO>()
                .returnResult().apply {
                    val exception = responseBody
                    Assertions.assertThat(exception).isNotNull
                    exception!!.messages.apply {
                        Assertions.assertThat(map { it.category }).containsOnly("Error")
                        Assertions.assertThat(map { it.code }).containsOnly("SERVER_ERROR")
                    }
                }
    }

}
