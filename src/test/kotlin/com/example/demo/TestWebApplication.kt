package com.example.demo


import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.http.HttpHeaders

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import javax.validation.constraints.Email

@SpringBootApplication
class TestWebApplication {

    @RestController
    @RequestMapping("/test")
    class TestController {


        @GetMapping("/acceptHeader")
        fun acceptHeader(@RequestHeader headers: HttpHeaders): Mono<Foo> = Foo("ojhavikas@gmail.com").toMono()

        @GetMapping("/customException")
        fun customException(@RequestHeader headers: HttpHeaders): Mono<CustomException> = CustomException(listOf(Feedback("some message",BusinessCode.INVALID_CONTENT))).toMono()

    }

    data class Foo(@field:Email val email: String?)
}
