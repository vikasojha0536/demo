package com.example.demo

import org.springframework.boot.autoconfigure.web.ResourceProperties
import org.springframework.boot.web.reactive.error.ErrorAttributes
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.codec.ServerCodecConfigurer

@Configuration(proxyBeanMethods = false)
class ApiConfiguration {


    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    fun errorWebHandler(errorAttributes: ErrorAttributes,
                        resourceProperties: ResourceProperties,
                        serverCodecConfigurer: ServerCodecConfigurer,
                        appContext: ApplicationContext): ErrorWebExceptionHandler {
        return CustomErrorWebExceptionHandler(errorAttributes, resourceProperties, serverCodecConfigurer, appContext)
    }
}