package com.example.demo

enum class TechnicalCode(override val status: Int, override val label: String) : ExceptionCode {

    ERROR_INTERNAL(500, "Internal server error. Typically a server bug");

    override val key = name
}