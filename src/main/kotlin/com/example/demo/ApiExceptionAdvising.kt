package com.example.demo


import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.server.NotAcceptableStatusException


@ControllerAdvice
class ApiExceptionAdvising {

    private val logger = LoggerFactory.getLogger(ApiExceptionAdvising::class.java)

    @ExceptionHandler(NotAcceptableStatusException::class)
    fun handle(exception: NotAcceptableStatusException) = map()


    @ExceptionHandler(CustomException::class)
    fun handle(exception: CustomException) = mapIntoInternal()

    fun map(): ResponseEntity<AnotherExceptionDTO> {
        return ResponseEntity(AnotherExceptionDTO(listOf(MessageDTO("Error", "FORMAT_ERROR", "accept header bad","accept"))), HttpStatus.BAD_REQUEST)

    }

    fun mapIntoInternal(): ResponseEntity<AnotherExceptionDTO> {
        return ResponseEntity(AnotherExceptionDTO(listOf(MessageDTO("Error", "SERVER_ERROR", "server_error",null))), HttpStatus.INTERNAL_SERVER_ERROR)

    }

}