package com.example.demo

data class AnotherExceptionDTO(val messages: List<MessageDTO> = emptyList())

data class MessageDTO(val category: String, val code: String, val text: String, val path: String?)