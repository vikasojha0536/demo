package com.example.demo

import java.io.Serializable

/**
 * Interface for different exception types
 * @see Serializable
 */
interface ExceptionCode : Serializable {

    val status: Int

    val key: String

    val label: String

}