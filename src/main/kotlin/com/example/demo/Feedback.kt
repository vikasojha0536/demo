package com.example.demo


class Feedback(val message: String,
               val code: ExceptionCode) {


    fun toException() = CustomException(listOf(this))
}