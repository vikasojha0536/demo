package com.example.demo



enum class BusinessCode(override val status: Int, override val label: String) : ExceptionCode {

    INVALID_CONTENT(400, "Invalid request payload"),
    MISSING_REQUIRED_QUERY_PARAMETER(400, "A required query parameter was not specified for this request"),
    MISSING_REQUIRED_HEADER(400, "A required HTTP header was not specified"),
    INVALID_QUERY_PARAMETER_VALUE(400, "An invalid value was specified for one of the query parameters in the request URI"),
    INVALID_HEADER_VALUE(400, "The value provided for one of the HTTP headers was not in the correct format"),
    OUT_OF_RANGE_QUERY_PARAMETER_VALUE(400, "A query parameter specified in the request URI is outside the permissible range"),
    RESOURCE_NOT_FOUND(404, "The specified resource does not exist"),
    UNSUPPORTED_HTTP_VERB(405, "The resource doesn't support the specified HTTP verb"),
    RESOURCE_ALREADY_EXIST(409, "The specified resource already exists"),
    INVALID_RANGE(416, "The range specified is invalid for the current size of the resource");

    override val key = name
}