package com.example.demo


import org.springframework.boot.autoconfigure.web.ResourceProperties
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler
import org.springframework.boot.web.reactive.error.ErrorAttributes
import org.springframework.context.ApplicationContext
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.*
import org.springframework.http.MediaType
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.web.reactive.function.server.HandlerFunction
import org.springframework.web.reactive.function.server.RequestPredicates.all
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.RouterFunctions.route
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.server.ResponseStatusException
import reactor.core.publisher.Mono

/**
 * Catches exceptions thrown by Spring Webflux before hitting a controller
 * @see AbstractErrorWebExceptionHandler
 */
class CustomErrorWebExceptionHandler(errorAttributes: ErrorAttributes,
                                     resourceProperties: ResourceProperties,
                                     serverCodecConfigurer: ServerCodecConfigurer,
                                     context: ApplicationContext)
    : AbstractErrorWebExceptionHandler(errorAttributes, resourceProperties, context) {

    init {
        setMessageReaders(serverCodecConfigurer.readers)
        setMessageWriters(serverCodecConfigurer.writers)
    }

    override fun getRoutingFunction(errorAttributes: ErrorAttributes?): RouterFunction<ServerResponse> {
        return route(all(), HandlerFunction { it.toErrorResponse() })
    }

    private fun ServerRequest.toErrorResponse() = toDxpException().toErrorResponse(this)

    private fun ServerRequest.toDxpException() = optionalCustomException() ?: when (status) {
        BAD_REQUEST -> errorBadRequest()
        NOT_FOUND -> errorNotFound()
        METHOD_NOT_ALLOWED -> errorMethodNotAllowed()
        else -> errorInternal()
    }

    private fun CustomException.toErrorResponse(serverRequest: ServerRequest): Mono<ServerResponse> {
        return ServerResponse.status(INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON).bodyValue(CustomException(listOf(Feedback("Unable to map error status", TechnicalCode.ERROR_INTERNAL))))
    }

    private fun ServerRequest.optionalCustomException() = err as? CustomException

    private fun ServerRequest.errorNotFound(): CustomException {
        return Feedback("Path '$path' does not exists", BusinessCode.RESOURCE_NOT_FOUND).toException()
    }

    private fun errorBadRequest(): CustomException {
        return Feedback("Bad request", BusinessCode.RESOURCE_NOT_FOUND).toException()
    }

    private fun ServerRequest.errorMethodNotAllowed(): CustomException {
        val message = "Method '${method()}' is not allowed on path '$path'"
        return Feedback(message, BusinessCode.UNSUPPORTED_HTTP_VERB).toException()
    }

    private fun errorInternal(): CustomException {
        return Feedback("Unable to map error status", TechnicalCode.ERROR_INTERNAL).toException()
    }

    private fun ServerRequest.unknownBadRequest(cause: Throwable): CustomException {
        return Feedback("Bad request on '$path' for unknown reasons", BusinessCode.INVALID_CONTENT).toException()
    }

    private val ServerRequest.status: HttpStatus? get() = (err as? ResponseStatusException)?.status
    private val ServerRequest.err: Throwable get() = getError(this)
    private val ServerRequest.path: String get() = path()
}
